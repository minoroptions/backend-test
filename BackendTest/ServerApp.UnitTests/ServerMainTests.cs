﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ServerApp.UnitTests
{
    [TestClass]
    public class ServerMainTests
    {
        [TestMethod]
        public void RegisterUser_ThreeIncomingRegisters_SuccessSuccessFail()
        {
            // Arrange
            var serverMain = new ServerMain();

            // Act, our server only support 2 registered client. The other must wait until the slot is available.
            var result1 = serverMain.RegisterUser(new ConnectedClient(), "Name1");
            var result2 = serverMain.RegisterUser(new ConnectedClient(), "Name2");
            var result3 = serverMain.RegisterUser(new ConnectedClient(), "Name3");

            // Assert
            Assert.IsTrue(result1.IsSuccess);
            Assert.IsTrue(result2.IsSuccess);
            Assert.IsFalse(result3.IsSuccess);
        }

        [TestMethod]
        public void GetUser_RegisteredUserAndNotRegisteredUser_SuccessFail()
        {
            // Arrange
            var serverMain = new ServerMain();
            var user = serverMain.RegisterUser(new ConnectedClient(), "Name1");

            // Act
            var resultRegisteredUser = serverMain.GetUser(user.UserId);
            var resultNotRegisteredUser = serverMain.GetUser(Guid.NewGuid());

            // Assert
            Assert.IsTrue(resultRegisteredUser.IsSuccess);
            Assert.IsFalse(resultNotRegisteredUser.IsSuccess);
        }

        #region Moves

        [TestMethod]
        public void MoveUp_MoveToBlackAndMoveOverOpponent_SuccessFail()
        {
            // Arrange
            var serverMain = new ServerMain();
            var user1 = serverMain.RegisterUser(new ConnectedClient(), "Name1");
            serverMain.Test_ForceSetUserPos(1, 1, 0, 0);

            // Act
            var result1 = serverMain.MoveUp(user1.UserId);

            // Assert
            Assert.IsTrue(result1.IsSuccess);

            // Arrange again, user2 is positioned above user1.
            var user2 = serverMain.RegisterUser(new ConnectedClient(), "Name2");
            serverMain.Test_ForceSetUserPos(1, 1, 1, 0);

            // Act
            var result2 = serverMain.MoveUp(user1.UserId);

            // Assert
            Assert.IsFalse(result2.IsSuccess);
        }

        [TestMethod]
        public void MoveDown_MoveToBlackAndMoveOverOpponent_SuccessFail()
        {
            // Arrange
            var serverMain = new ServerMain();
            var user1 = serverMain.RegisterUser(new ConnectedClient(), "Name1");
            serverMain.Test_ForceSetUserPos(1, 1, 0, 0);

            // Act
            var result1 = serverMain.MoveDown(user1.UserId);

            // Assert
            Assert.IsTrue(result1.IsSuccess);

            // Arrange again, user2 is positioned below user1.
            var user2 = serverMain.RegisterUser(new ConnectedClient(), "Name2");
            serverMain.Test_ForceSetUserPos(1, 1, 1, 2);

            // Act
            var result2 = serverMain.MoveDown(user1.UserId);

            // Assert
            Assert.IsFalse(result2.IsSuccess);
        }

        [TestMethod]
        public void MoveLeft_MoveToBlackAndMoveOverOpponent_SuccessFail()
        {
            // Arrange
            var serverMain = new ServerMain();
            var user1 = serverMain.RegisterUser(new ConnectedClient(), "Name1");
            serverMain.Test_ForceSetUserPos(1, 1, 0, 0);

            // Act
            var result1 = serverMain.MoveLeft(user1.UserId);

            // Assert
            Assert.IsTrue(result1.IsSuccess);

            // Arrange again, user2 is positioned on the left of user1.
            var user2 = serverMain.RegisterUser(new ConnectedClient(), "Name2");
            serverMain.Test_ForceSetUserPos(1, 1, 0, 1);

            // Act
            var result2 = serverMain.MoveLeft(user1.UserId);

            // Assert
            Assert.IsFalse(result2.IsSuccess);
        }

        [TestMethod]
        public void MoveRight_MoveToBlackAndMoveOverOpponent_SuccessFail()
        {
            // Arrange
            var serverMain = new ServerMain();
            var user1 = serverMain.RegisterUser(new ConnectedClient(), "Name1");
            serverMain.Test_ForceSetUserPos(1, 1, 0, 0);

            // Act
            var result1 = serverMain.MoveRight(user1.UserId);

            // Assert
            Assert.IsTrue(result1.IsSuccess);

            // Arrange again, user2 is positioned on the right of user1.
            var user2 = serverMain.RegisterUser(new ConnectedClient(), "Name2");
            serverMain.Test_ForceSetUserPos(1, 1, 2, 1);

            // Act
            var result2 = serverMain.MoveRight(user1.UserId);

            // Assert
            Assert.IsFalse(result2.IsSuccess);
        }

        #endregion

        [TestMethod]
        public void GetPosition_RegisteredUserAndNotRegisteredUser_SuccessFail()
        {
            // Arrange
            var serverMain = new ServerMain();
            var user = serverMain.RegisterUser(new ConnectedClient(), "Name1");

            // Act
            var resultRegisteredUser = serverMain.GetPosition(user.UserId);
            var resultNotRegisteredUser = serverMain.GetPosition(Guid.NewGuid());

            // Assert
            Assert.IsTrue(resultRegisteredUser.IsSuccess);
            Assert.IsFalse(resultNotRegisteredUser.IsSuccess);
        }

        [TestMethod]
        public void GetNearest_OneUserAndTwoUsers_FailSuccess()
        {
            // Arrange
            var serverMain = new ServerMain();
            var user1 = serverMain.RegisterUser(new ConnectedClient(), "Name1");

            // Act
            var resultOneUser = serverMain.GetNearest(user1.UserId);

            // Assert
            Assert.IsFalse(resultOneUser.IsSuccess);

            // Arrange again with 2nd user.
            var user2 = serverMain.RegisterUser(new ConnectedClient(), "Name2");

            // Act
            var resultTwoUsers = serverMain.GetNearest(user2.UserId);

            // Assert
            Assert.IsTrue(resultTwoUsers.IsSuccess);
        }
    }
}
