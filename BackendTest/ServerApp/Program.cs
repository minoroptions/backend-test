﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var serverMain = new ServerMain();
            serverMain.Start();

            Console.WriteLine("Server Start RUNNING!!");
            Console.ReadLine();
        }
    }
}
