﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Collections.Concurrent;
using System.Threading;
using ClassLib;

namespace ServerApp
{
    public class ServerMain
    {
        #region Connected Clients

        private ConcurrentDictionary<ConnectedClient, int> connectedClients = new ConcurrentDictionary<ConnectedClient, int>();

        private ConnectedClient registeredClientPlayer1;

        private ConnectedClient registeredClientPlayer2;

        #endregion

        #region Grid Property

        public const int gridSizeX = 10;

        public const int gridSizeY = 10;

        #endregion

        #region Unit Test
        /// <summary>
        /// This is a test method for unit test only.
        /// </summary>
        /// <param name="posX1"></param>
        /// <param name="posY1"></param>
        /// <param name="posX2"></param>
        /// <param name="posY2"></param>
        public void Test_ForceSetUserPos(int posX1, int posY1, int posX2, int posY2)
        {
            if (registeredClientPlayer1 != null)
                registeredClientPlayer1.SetPlayerPos(posX1, posY1);

            if (registeredClientPlayer2 != null)
                registeredClientPlayer2.SetPlayerPos(posX2, posY2);
        }

        #endregion

        #region Network Related Methods

        public void Start(int port = 8888)
        {
            var endPoint = new IPEndPoint(IPAddress.Loopback, port);

            var socket = new Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            socket.Bind(endPoint);
            socket.Listen(128);

            // this is basically like an Update loop.
            //var updateTask = Task.Run(() => UpdateTask(socket));

            Task.Run(() => UpdateConnectClientsListener(socket));
        }

        private async void UpdateConnectClientsListener(Socket socket)
        {
            Console.WriteLine("ServerMain: Start Clients Listener");

            while (true)
            {
                var func1 = new Func<AsyncCallback, object, IAsyncResult>(socket.BeginAccept);
                var func2 = new Func<IAsyncResult, Socket>(socket.EndAccept);
                var clientSocket = await Task.Factory.FromAsync(func1, func2, null).ConfigureAwait(false);

                Console.WriteLine("ServerMain: Found new client connected. Create a new update task for it.");

                var connectedClient = new ConnectedClient(this, clientSocket);
                connectedClient.onErrorOccurred += OnConnectedClientRemove;

                do
                {
                    var succeed = connectedClients.TryAdd(connectedClient, 0);
                    if (succeed)
                        break;

                    Thread.Sleep(1);

                } while (true);

                //var clientTask = Task.Run(() => UpdateClientListener(socket));
            }
        }

        private void OnConnectedClientRemove(ConnectedClient client)
        {
            bool found;
            if (client.IsRegistered)
            {
                Console.WriteLine("ServerMain: registered client remove: " + client.UserID + ", " + client.PlayerName);

                found = TryGetOpponentClient(client, out ConnectedClient opponent);
                if (found)
                    opponent.OnOpponentUnregister(client.UserID);

                UnsetPlayer(client);
            }
            else
            {
                Console.WriteLine("ServerMain: unregistered client remove.");
            }

            client.Dispose();

            // this should not happen but just in case.
            found = connectedClients.ContainsKey(client);
            if (!found)
                return;

            while (true)
            {
                var succeed = connectedClients.TryRemove(client, out int value);
                if (succeed)
                    break;

                Thread.Sleep(1);
            }
        }

        private void UnsetPlayer(ConnectedClient client)
        {
            if (registeredClientPlayer1 == client)
            {
                registeredClientPlayer1.registeredData = null;
                registeredClientPlayer1 = null;
                return;
            }

            if (registeredClientPlayer2 == client)
            {
                registeredClientPlayer2.registeredData = null;
                registeredClientPlayer2 = null;
                return;
            }
        }

        #endregion


        #region APIs

        #region RegisterUser

        public RegisterResultMessage RegisterUser(ConnectedClient client, string playerName)
        {
            Console.WriteLine("ServerMain: API call: RegisterUser(" + playerName + ")");

            if (client == null || string.IsNullOrEmpty(playerName))
                return new RegisterResultMessage(Guid.Empty, -1, -1, ServerErrorCode.GetMessage("1000"), "1000");

            if (IsAlreadyAPlayer(client))
                return new RegisterResultMessage(Guid.Empty, -1, -1, ServerErrorCode.GetMessage("1001"), "1001");

            if (IsPlayerFull(client))
                return new RegisterResultMessage(Guid.Empty, -1, -1, ServerErrorCode.GetMessage("1002"), "1002");

            var opponent = SetPlayer(client);

            GetSpawnPosition(opponent, out int posX, out int posY);

            var registeredData = new RegisteredData()
            {
                userId = Guid.NewGuid(),
                playerName = playerName,
                posX = posX,
                posY = posY,
            };

            client.registeredData = registeredData;

            if (opponent != null)
            {
                (int posXOp, int posYOp) = opponent.GetPlayerPos();

                opponent.OnOpponentRegisterOrMove(client.UserID, client.PlayerName, posX, posY);
                client.OnOpponentRegisterOrMove(opponent.UserID, opponent.PlayerName, posXOp, posYOp);
            }

            return new RegisterResultMessage(registeredData.userId, posX, posY);
        }

        private bool IsAlreadyAPlayer(ConnectedClient client)
        {
            return IsAlreadyAPlayer(client.UserID);
        }

        private bool IsPlayerFull(ConnectedClient client)
        {
            return registeredClientPlayer1 != null && registeredClientPlayer2 != null;
        }

        private ConnectedClient SetPlayer(ConnectedClient client)
        {
            if (registeredClientPlayer1 == null)
            {
                registeredClientPlayer1 = client;
                return registeredClientPlayer2;
            }

            registeredClientPlayer2 = client;
            return registeredClientPlayer1;
        }

        private void GetSpawnPosition(ConnectedClient opponent, out int posX, out int posY)
        {
            var rand = new Random();
            while (true)
            {
                posX = rand.Next(0, gridSizeX);
                posY = rand.Next(0, gridSizeY);

                if (opponent == null || !opponent.IsPlayerAtPos(posX, posY))
                    break;
            }
        }

        #endregion

        #region Get User

        public GetUserResultMessage GetUser(Guid userId)
        {
            Console.WriteLine("ServerMain: API call: GetUser(" + userId + ")");

            var found = TryGetRegisteredClient(userId, out ConnectedClient client);
            if (!found)
                return new GetUserResultMessage(Guid.Empty, "ERROR", ServerErrorCode.GetMessage("2001"), "2001");

            return new GetUserResultMessage(client.UserID, client.PlayerName);
        }

        private bool TryGetRegisteredClient(Guid userId, out ConnectedClient client)
        {
            client = null;

            if (userId == Guid.Empty)
                return false;

            if (registeredClientPlayer1 != null && registeredClientPlayer1.UserID == userId)
            {
                client = registeredClientPlayer1;
                return true;
            }

            if (registeredClientPlayer2 != null && registeredClientPlayer2.UserID == userId)
            {
                client = registeredClientPlayer2;
                return true;
            }

            return false;
        }

        #endregion

        #region Move

        public MoveResultMessage MoveUp(Guid userId)
        {
            Console.WriteLine("ServerMain: API call: MoveUp(" + userId +")");

            var found = TryGetRegisteredClient(userId, out ConnectedClient client);
            if (!found)
                return new MoveResultMessage(Guid.Empty, "ERROR", -1, -1, ServerErrorCode.GetMessage("2001"), "2001");

            (int posX, int posY) = client.GetPlayerPos();
            posY--;

            return MoveByPos(client, posX, posY);
        }

        public MoveResultMessage MoveDown(Guid userId)
        {
            Console.WriteLine("ServerMain: API call: MoveDown(" + userId + ")");

            var found = TryGetRegisteredClient(userId, out ConnectedClient client);
            if (!found)
                return new MoveResultMessage(Guid.Empty, "ERROR", -1, -1, ServerErrorCode.GetMessage("2001"), "2001");

            (int posX, int posY) = client.GetPlayerPos();
            posY++;

            return MoveByPos(client, posX, posY);
        }

        public MoveResultMessage MoveLeft(Guid userId)
        {
            Console.WriteLine("ServerMain: API call: MoveLeft(" + userId + ")");

            var found = TryGetRegisteredClient(userId, out ConnectedClient client);
            if (!found)
                return new MoveResultMessage(Guid.Empty, "ERROR", -1, -1, ServerErrorCode.GetMessage("2001"), "2001");

            (int posX, int posY) = client.GetPlayerPos();
            posX--;

            return MoveByPos(client, posX, posY);
        }

        public MoveResultMessage MoveRight(Guid userId)
        {
            Console.WriteLine("ServerMain: API call: MoveRight(" + userId + ")");

            var found = TryGetRegisteredClient(userId, out ConnectedClient client);
            if (!found)
                return new MoveResultMessage(Guid.Empty, "ERROR", -1, -1, ServerErrorCode.GetMessage("2001"), "2001");

            (int posX, int posY) = client.GetPlayerPos();
            posX++;

            return MoveByPos(client, posX, posY);
        }

        private MoveResultMessage MoveByPos(ConnectedClient client, int posX, int posY)
        {
            if (posX < 0 || posX >= gridSizeX || posY < 0 || posY >= gridSizeY)
                return new MoveResultMessage(Guid.Empty, "ERROR", -1, -1, ServerErrorCode.GetMessage("2002"), "2002");

            var foundOp = TryGetOpponentClient(client, out ConnectedClient opponent);
            if (foundOp)
            {
                (int posXOP, int posYOP) = opponent.GetPlayerPos();
                if (posX == posXOP && posY == posYOP)
                    return new MoveResultMessage(Guid.Empty, "ERROR", -1, -1, ServerErrorCode.GetMessage("2003"), "2003");

                opponent.OnOpponentRegisterOrMove(client.UserID, client.PlayerName, posX, posY);
            }

            client.SetPlayerPos(posX, posY);
            return new MoveResultMessage(client.UserID, client.PlayerName, posX, posY);
        }

        private bool IsAlreadyAPlayer(Guid userId)
        {
            if (userId == Guid.Empty)
                return false;

            if (registeredClientPlayer1.UserID == userId)
                return true;

            if (registeredClientPlayer2.UserID == userId)
                return true;

            return false;
        }

        private bool TryGetOpponentClient(ConnectedClient yourClient, out ConnectedClient opponent)
        {
            opponent = null;

            if (yourClient == null)
                return false;

            if (registeredClientPlayer1 == yourClient && registeredClientPlayer2 != null)
            {
                opponent = registeredClientPlayer2;
                return true;
            }

            if (registeredClientPlayer2 == yourClient && registeredClientPlayer1 != null)
            {
                opponent = registeredClientPlayer1;
                return true;
            }

            return false;
        }

        #endregion

        #region GetPosition

        public GetPositionResultMessage GetPosition(Guid userId)
        {
            Console.WriteLine("ServerMain: API call: GetPosition(" + userId + ")");

            var found = TryGetRegisteredClient(userId, out ConnectedClient client);
            if (!found)
                return new GetPositionResultMessage(Guid.Empty, -1, -1, ServerErrorCode.GetMessage("2001"), "2001");

            (int posX, int posY) = client.GetPlayerPos();

            return new GetPositionResultMessage(userId, posX, posY);
        }

        #endregion

        #region GetNearest

        public GetNearestResultMessage GetNearest(Guid userId)
        {
            Console.WriteLine("ServerMain: API call: GetNearest(" + userId + ")");

            var found = TryGetRegisteredClient(userId, out ConnectedClient client);
            if (!found)
                return new GetNearestResultMessage(Guid.Empty, -1, ServerErrorCode.GetMessage("2001"), "2001");

            found = TryGetOpponentClient(client, out ConnectedClient opponent);
            if (!found)
                return new GetNearestResultMessage(Guid.Empty, -1, ServerErrorCode.GetMessage("3001"), "3001");

            (int posXP, int posYP) = client.GetPlayerPos();
            (int posXOp, int posYOp) = opponent.GetPlayerPos();

            var xDistance = Math.Abs(posXP - posXOp);
            var yDistance = Math.Abs(posYP - posYOp);
            var totalDis = xDistance + yDistance;

            return new GetNearestResultMessage(userId, totalDis);
        }

        #endregion

        #endregion
    }
}
