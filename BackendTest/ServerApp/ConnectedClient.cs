﻿using ClassLib;
using System;
using System.Collections.Concurrent;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace ServerApp
{
    public class ConnectedClient
    {
        public ServerMain serverMain;

        public RegisteredData registeredData;

        public Socket clientSocket;

        public NetworkStream networkStream;

        #region User related methods

        public bool IsRegistered
        {
            get { return registeredData != null; }
        }

        public Guid UserID
        {
            get
            {
                if (registeredData == null)
                    return Guid.Empty;

                return registeredData.userId;
            }
        }

        public string PlayerName
        {
            get
            {
                if (registeredData == null)
                    return "ERROR";

                return registeredData.playerName;
            }
        }

        public (int posX, int posY) GetPlayerPos()
        {
            if (registeredData == null)
                return (-1, -1);

            return (registeredData.posX, registeredData.posY);
        }

        public void SetPlayerPos(int posX, int posY)
        {
            if (registeredData == null)
                return;

            registeredData.posX = posX;
            registeredData.posY = posY;
        }

        public bool IsPlayerAtPos(int posX, int posY)
        {
            if (registeredData == null)
                return false;

            return registeredData.posX == posX && registeredData.posY == posY;
        }

        #endregion


        private ConcurrentQueue<MyMessageBase> toSendMessageQueue = new ConcurrentQueue<MyMessageBase>();

        private ConcurrentQueue<MyMessageBase> receivedMessageQueue = new ConcurrentQueue<MyMessageBase>();

        public Action<ConnectedClient> onErrorOccurred;

        /// <summary>
        /// Use for testing purpose only.
        /// </summary>
        public ConnectedClient()
        {

        }

        public ConnectedClient(ServerMain serverMain, Socket clientSocket)
        {
            this.serverMain = serverMain;
            this.clientSocket = clientSocket;
            networkStream = new NetworkStream(clientSocket, true);

            //var token = source.Token;

            _ = Task.Run(() => ClientListenerUpdate());
            _ = Task.Run(() => ClientSenderUpdate());
            _ = Task.Run(() => ProcessDataUpdate());
        }

        public void Dispose()
        {
            try
            {
                networkStream.Close();
                clientSocket.Shutdown(SocketShutdown.Both);
            }
            catch (Exception ex)
            {
            }

            networkStream = null;
            clientSocket = null;

            registeredData = null;
            onErrorOccurred = null;

            toSendMessageQueue = null;
            receivedMessageQueue = null;

            serverMain = null;
        }

        #region Callbacks

        public void OnOpponentUnregister(Guid opponentUserId)
        {
            if (registeredData == null)
                return;

            var message = new DisconnectMessage(opponentUserId);
            toSendMessageQueue.Enqueue(message);
        }

        public void OnOpponentRegisterOrMove(Guid opponentUserId, string playerName, int posX, int posY)
        {
            if (registeredData == null)
                return;

            var message = new MoveResultMessage(opponentUserId, playerName, posX, posY);
            toSendMessageQueue.Enqueue(message);
        }

        #endregion

        #region Updates

        private async void ClientListenerUpdate()
        {
            var buffer = new byte[1024];

            while (true)
            {
                try
                {
                    if (receivedMessageQueue == null)
                        return;

                    int bytesRead = await networkStream.ReadAsync(buffer, 0, buffer.Length).ConfigureAwait(false);
                    if (bytesRead == 0)
                        continue;

                    var incomingMessage = MyMessageHelper.GetMyMessage(buffer, bytesRead);
                    receivedMessageQueue.Enqueue(incomingMessage);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ConnectedClient: exception happens when try to read message from networkStream.");
                    onErrorOccurred?.Invoke(this);
                    break;
                }
            }
        }

        private async void ClientSenderUpdate()
        {
            while (true)
            {
                try
                {
                    if (toSendMessageQueue == null)
                        return;

                    while (toSendMessageQueue.Count > 0)
                    {
                        var succeed = toSendMessageQueue.TryDequeue(out MyMessageBase message);
                        if (!succeed)
                        {
                            Thread.Sleep(1);
                            continue;
                        }

                        var buffer = message.GetByteArray();
                        await networkStream.WriteAsync(buffer, 0, buffer.Length).ConfigureAwait(false);

                        if (toSendMessageQueue == null)
                            return;
                    }

                    Thread.Sleep(5);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ConnectedClient: exception happens when try to write message.");
                    onErrorOccurred?.Invoke(this);
                    break;
                }
            }
        }

        private void ProcessDataUpdate()
        {
            while (true)
            {
                try
                {
                    if (receivedMessageQueue == null)
                        return;

                    while (receivedMessageQueue.Count > 0)
                    {
                        var succeed = receivedMessageQueue.TryDequeue(out MyMessageBase message);
                        if (!succeed)
                        {
                            Thread.Sleep(1);
                            continue;
                        }

                        ProcessReceivedMessage(message);

                        if (receivedMessageQueue == null)
                            return;
                    }

                    Thread.Sleep(5);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("ConnectedClient: exception happens when try to ProcessDataUpdate(), ex: " + ex);
                    onErrorOccurred?.Invoke(this);
                    break;
                }
            }
        }

        #endregion

        #region Process Messages

        private void ProcessReceivedMessage(MyMessageBase message)
        {
            switch (message.Type)
            {
                case MyMessageType.Disconnect:
                    ProcessDisconnectMessage(message as DisconnectMessage);
                    break;

                case MyMessageType.RegisterUser:
                    ProcessRegisterUserMessage(message as RegisterUserMessage);
                    break;

                case MyMessageType.GetUser:
                    ProcessGetUserMessage(message as GetUserMessage);
                    break;

                case MyMessageType.Move:
                    ProcessMoveMessage(message as MoveMessage);
                    break;

                case MyMessageType.GetPosition:
                    ProcessGetPositionMessage(message as GetPositionMessage);
                    break;

                case MyMessageType.GetNearest:
                    ProcessGetNearestMessage(message as GetNearestMessage);
                    break;

                default:
                    throw new Exception("ConnectedClient: not supported message type: " + message.Type);
            }
        }

        private void ProcessDisconnectMessage(DisconnectMessage message)
        {
            // NOTE: currently there is not workflow for client to send disconnect message to the server.
            throw new NotImplementedException("ProcessDisconnectMessage() is not implemented.");
        }

        private void ProcessRegisterUserMessage(RegisterUserMessage message)
        {
            var result = serverMain.RegisterUser(this, message.PlayerName);
            toSendMessageQueue.Enqueue(result);
        }

        private void ProcessGetUserMessage(GetUserMessage message)
        {
            var result = serverMain.GetUser(message.UserID);
            toSendMessageQueue.Enqueue(result);
        }

        private void ProcessMoveMessage(MoveMessage message)
        {
            MoveResultMessage result = null;

            switch (message.Direction)
            {
                case MoveDir.Up:
                    result = serverMain.MoveUp(message.UserID);
                    break;

                case MoveDir.Down:
                    result = serverMain.MoveDown(message.UserID);
                    break;

                case MoveDir.Left:
                    result = serverMain.MoveLeft(message.UserID);
                    break;

                case MoveDir.Right:
                    result = serverMain.MoveRight(message.UserID);
                    break;

                default:
                    Console.WriteLine("ConnectedClient: MoveMessage has unhandle MoveDir: " + message.Direction);
                    result = new MoveResultMessage(Guid.Empty, "ERROR", -1, -1, ServerErrorCode.GetMessage("2004"), "2004");
                    break;
            }

            toSendMessageQueue.Enqueue(result);
        }

        private void ProcessGetPositionMessage(GetPositionMessage message)
        {
            var result = serverMain.GetPosition(message.UserID);
            toSendMessageQueue.Enqueue(result);
        }

        private void ProcessGetNearestMessage(GetNearestMessage message)
        {
            var result = serverMain.GetNearest(message.UserID);
            toSendMessageQueue.Enqueue(result);
        }

        #endregion
    }
}
