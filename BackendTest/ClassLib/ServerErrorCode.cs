﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLib
{
    public class ServerErrorCode
    {
        public const string noError = "0000";

        public static string GetMessage(string errorCode)
        {
            switch (errorCode)
            {
                // general error.
                case "1000":
                    return "Message error";

                #region RegisterUser

                case "1001":
                    return "Already a user";

                case "1002":
                    return "Server is full";

                #endregion

                case "2001":
                    return "Not a player";

                #region GetUser and Move

                case "2002":
                    return "Cannot move out of bound";

                case "2003":
                    return "Cannot move over opponent";

                case "2004":
                    return "MoveDir not found";

                #endregion

                #region GetNearest

                case "3001":
                    return "Opponent not found";

                #endregion

                default:
                    return string.Empty;
            }
        }
    }
}
