﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLib
{
    public class MyMessageHelper
    {
        public const char paramSep = '|';

        public const int maxPlayerNameLength = 15;

        public static MyMessageBase GetMyMessage(byte[] response, int bytesRead)
        {
            if (bytesRead == 0)
                throw new Exception(
                    "MessageHelper: bytesRead is zero. The response is invalid. Maybe the socket is already disconnected.");

            var responseStr = System.Text.Encoding.UTF8.GetString(response, 0, bytesRead);
            return GetMyMessage(responseStr);
        }

        public static MyMessageBase GetMyMessage(string responseStr)
        {
            var strArray = responseStr.Split(paramSep);

            if (strArray == null || strArray.Length == 0)
                throw new Exception(
                    "MessageHelper: strArray from responseStr: " + responseStr + " is null or length zero. " +
                    "The response is invalid. Maybe the socket is already disconnected.");

            return CreateMessage(strArray);
        }

        private static MyMessageBase CreateMessage(string[] strArray)
        {
            var messageTypeStr = strArray[0];
            var succeed = Int32.TryParse(messageTypeStr, out int result);
            if (!succeed)
                throw new Exception("MessageHelper: fail to parse messageTypeStr: " + messageTypeStr + " to an int32.");

            var messageType = (MyMessageType)result;

            switch (messageType)
            {
                case MyMessageType.Disconnect:
                    return new DisconnectMessage(strArray);

                case MyMessageType.RegisterUser:
                    return new RegisterUserMessage(strArray);

                case MyMessageType.RegisterResult:
                    return new RegisterResultMessage(strArray);

                case MyMessageType.GetUser:
                    return new GetUserMessage(strArray);

                case MyMessageType.GetUserResult:
                    return new GetUserResultMessage(strArray);

                case MyMessageType.GetPosition:
                    return new GetPositionMessage(strArray);

                case MyMessageType.GetPositionResult:
                    return new GetPositionResultMessage(strArray);

                case MyMessageType.GetNearest:
                    return new GetNearestMessage(strArray);

                case MyMessageType.GetNearestResult:
                    return new GetNearestResultMessage(strArray);

                case MyMessageType.Move:
                    return new MoveMessage(strArray);

                case MyMessageType.MoveResult:
                    return new MoveResultMessage(strArray);

                default:
                    throw new NotImplementedException("MessageHelper: not implement CreateMessage(" + messageType + ")");
            }
        }
    }
}
