﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLib
{
    public class GetNearestResultMessage : MyMessageBase
    {
        public override MyMessageType Type => MyMessageType.GetNearestResult;

        private Guid userId;
        public Guid UserID => userId;

        private int distance;
        public int Distance => distance;

        #region Error Report

        private string errorMessage;
        public string ErrorMessage => errorMessage;

        private string errorCode;
        public string ErrorCode => errorCode;

        #endregion

        public bool IsSuccess
        {
            get { return errorCode == ServerErrorCode.noError; }
        }

        public GetNearestResultMessage(Guid userId, int distance, string errorMessage = "NONE", string errorCode = ServerErrorCode.noError)
        {
            this.userId = userId;
            this.distance = distance;
            this.errorMessage = errorMessage;
            this.errorCode = errorCode;
        }

        public GetNearestResultMessage(string[] strArray)
        {
            if (strArray == null || strArray.Length < 5)
                throw new IndexOutOfRangeException("GetNearestResultMessage: strArray must be 5 or more.");

            var idStr = strArray[1];
            var succeed = Guid.TryParse(idStr, out Guid result);
            if (!succeed)
                throw new Exception("GetNearestResultMessage: fail to parse string: " + idStr + " to GUID.");

            userId = result;
            Int32.TryParse(strArray[2], out distance);
            errorMessage = strArray[3];
            errorCode = strArray[4];
        }

        public override byte[] GetByteArray()
        {
            var sep = MyMessageHelper.paramSep;

            var type = ((int)Type).ToString();
            string str = type + sep + userId.ToString() +
                sep + distance.ToString() +
                sep + errorMessage + sep + errorCode;
            var buffer = System.Text.Encoding.UTF8.GetBytes(str);
            return buffer;
        }
    }
}
