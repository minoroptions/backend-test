﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLib
{
    public enum MyMessageType
    {
        None = 0,
        Disconnect,
        RegisterUser,
        RegisterResult,
        GetUser,
        GetUserResult,
        Move,
        MoveResult,
        GetPosition,
        GetPositionResult,
        GetNearest,
        GetNearestResult,
    }

    public abstract class MyMessageBase
    {
        public abstract MyMessageType Type { get; }

        public abstract byte[] GetByteArray();
    }
}
