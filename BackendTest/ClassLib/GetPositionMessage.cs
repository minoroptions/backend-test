﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLib
{
    public class GetPositionMessage : MyMessageBase
    {
        public override MyMessageType Type => MyMessageType.GetPosition;

        private Guid userId;
        public Guid UserID => userId;

        public GetPositionMessage(Guid id)
        {
            this.userId = id;
        }

        public GetPositionMessage(string[] strArray)
        {
            if (strArray == null || strArray.Length < 2)
                throw new IndexOutOfRangeException("GetPositionMessage: strArray must be 2 or more.");

            var idStr = strArray[1];
            var succeed = Guid.TryParse(idStr, out Guid result);
            if (!succeed)
                throw new Exception("GetPositionMessage: fail to parse string: " + idStr + " to GUID.");

            userId = result;
        }

        public override byte[] GetByteArray()
        {
            var sep = MyMessageHelper.paramSep;

            var type = ((int)Type).ToString();
            string str = type + sep + userId.ToString();
            var buffer = System.Text.Encoding.UTF8.GetBytes(str);
            return buffer;
        }
    }
}
