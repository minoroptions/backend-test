﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLib
{
    public class GetUserResultMessage : MyMessageBase
    {
        public override MyMessageType Type => MyMessageType.GetUserResult;

        private Guid userId;
        public Guid UserId => userId;

        private string playerName;
        public string PlayerName => playerName;

        #region Error Report

        private string errorMessage;
        public string ErrorMessage => errorMessage;

        private string errorCode;
        public string ErrorCode => errorCode;

        #endregion

        public bool IsSuccess
        {
            get { return errorCode == ServerErrorCode.noError; }
        }

        public GetUserResultMessage(Guid userId, string playerName, string errorMessage = "NONE", string errorCode = ServerErrorCode.noError)
        {
            this.userId = userId;
            this.playerName = playerName;
            this.errorMessage = errorMessage;
            this.errorCode = errorCode;
        }

        public GetUserResultMessage(string[] strArray)
        {
            if (strArray == null || strArray.Length < 5)
                throw new IndexOutOfRangeException("GetUserResultMessage: strArray must be 6 or more.");

            var idStr = strArray[1];
            var succeed = Guid.TryParse(idStr, out Guid result);
            if (!succeed)
                throw new Exception("GetUserResultMessage: fail to parse string: " + idStr + " to GUID.");

            userId = result;
            playerName = strArray[2];
            errorMessage = strArray[3];
            errorCode = strArray[4];
        }

        public override byte[] GetByteArray()
        {
            var sep = MyMessageHelper.paramSep;

            var type = ((int)Type).ToString();
            string str = type + sep + userId.ToString() +
                sep + playerName +
                sep + errorMessage + sep + errorCode;
            var buffer = System.Text.Encoding.UTF8.GetBytes(str);
            return buffer;
        }
    }
}
