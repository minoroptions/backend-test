﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLib
{
    public class RegisterUserMessage : MyMessageBase
    {
        public override MyMessageType Type => MyMessageType.RegisterUser;

        private string playerName;

        public string PlayerName => playerName;

        public RegisterUserMessage(string playerName)
        {
            if (playerName.Length > MyMessageHelper.maxPlayerNameLength)
                throw new IndexOutOfRangeException(
                    "RegisterUserMessage: playerName length must not exceed " + MyMessageHelper.maxPlayerNameLength);

            this.playerName = playerName;
        }

        public RegisterUserMessage(string[] strArray)
        {
            if (strArray == null || strArray.Length < 2)
                throw new IndexOutOfRangeException("RegisterUserMessage: strArray must be 2 or more.");

            playerName = strArray[1];
        }

        public override byte[] GetByteArray()
        {
            var sep = MyMessageHelper.paramSep;

            var type = ((int)Type).ToString();
            string str = type + sep + PlayerName;
            var buffer = System.Text.Encoding.UTF8.GetBytes(str);
            return buffer;
        }
    }
}
