﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLib
{
    public class RegisterResultMessage : MyMessageBase
    {
        public override MyMessageType Type => MyMessageType.RegisterResult;

        private Guid userId;
        public Guid UserId => userId;

        private int posX;
        public int PosX => posX;

        private int posY;
        public int PosY => posY;

        #region Error Report

        private string errorMessage;
        public string ErrorMessage => errorMessage;

        private string errorCode;
        public string ErrorCode => errorCode;

        #endregion

        public bool IsSuccess
        {
            get { return errorCode == ServerErrorCode.noError;  }
        }

        public RegisterResultMessage(Guid userId, int posX, int posY, string errorMessage = "NONE", string errorCode = ServerErrorCode.noError)
        {
            this.userId = userId;
            this.posX = posX;
            this.posY = posY;
            this.errorMessage = errorMessage;
            this.errorCode = errorCode;
        }

        public RegisterResultMessage(string[] strArray)
        {
            if (strArray == null || strArray.Length < 6)
                throw new IndexOutOfRangeException("RegisterResultMessage: strArray must be 6 or more.");

            var idStr = strArray[1];
            var succeed = Guid.TryParse(idStr, out Guid result);
            if (!succeed)
                throw new Exception("RegisterResultMessage: fail to parse string: " + idStr + " to GUID.");

            userId = result;
            Int32.TryParse(strArray[2], out posX);
            Int32.TryParse(strArray[3], out posY);
            errorMessage = strArray[4];
            errorCode = strArray[5];
        }

        public override byte[] GetByteArray()
        {
            var sep = MyMessageHelper.paramSep;

            var type = ((int)Type).ToString();
            string str = type + sep + userId.ToString() +
                sep + posX.ToString() + sep + posY.ToString() +
                sep + errorMessage + sep + errorCode;
            var buffer = System.Text.Encoding.UTF8.GetBytes(str);
            return buffer;
        }
    }
}
