﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLib
{
    public enum MoveDir
    {
        None,
        Left,
        Right,
        Up,
        Down,
    }

    public class MoveMessage : MyMessageBase
    {
        public override MyMessageType Type => MyMessageType.Move;

        private Guid userId;
        public Guid UserID => userId;

        private MoveDir direction;
        public MoveDir Direction => direction;

        public MoveMessage(Guid userId, MoveDir direction)
        {
            this.userId = userId;
            this.direction = direction;
        }

        public MoveMessage(string[] strArray)
        {
            if (strArray == null || strArray.Length < 3)
                throw new IndexOutOfRangeException("MoveMessage: strArray must be 3 or more.");

            var idStr = strArray[1];
            var succeed = Guid.TryParse(idStr, out Guid result);
            if (!succeed)
                throw new Exception("MoveMessage: fail to parse string: " + idStr + " to GUID.");

            userId = result;

            Int32.TryParse(strArray[2], out int dir);
            direction = (MoveDir)dir;
        }

        public override byte[] GetByteArray()
        {
            var sep = MyMessageHelper.paramSep;

            var type = ((int)Type).ToString();
            var dir = ((int)direction).ToString();
            string str = type + sep + userId.ToString() + sep + dir;
            var buffer = System.Text.Encoding.UTF8.GetBytes(str);
            return buffer;
        }
    }
}
