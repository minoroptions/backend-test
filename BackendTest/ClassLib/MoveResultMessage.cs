﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLib
{
    public class MoveResultMessage : MyMessageBase
    {
        public override MyMessageType Type => MyMessageType.MoveResult;

        private Guid userId;
        public Guid UserId => userId;

        private string playerName;
        public string PlayerName => playerName;

        private int posX;
        public int PosX => posX;

        private int posY;
        public int PosY => posY;

        #region Error Report

        private string errorMessage;
        public string ErrorMessage => errorMessage;

        private string errorCode;
        public string ErrorCode => errorCode;

        #endregion

        public bool IsSuccess
        {
            get { return errorCode == ServerErrorCode.noError; }
        }

        public MoveResultMessage(Guid userId, string playerName, int posX, int posY, string errorMessage = "NONE", string errorCode = ServerErrorCode.noError)
        {
            this.userId = userId;
            this.playerName = playerName;
            this.posX = posX;
            this.posY = posY;
            this.errorMessage = errorMessage;
            this.errorCode = errorCode;
        }

        public MoveResultMessage(string[] strArray)
        {
            if (strArray == null || strArray.Length < 7)
                throw new IndexOutOfRangeException("MoveResultMessage: strArray must be 6 or more.");

            var idStr = strArray[1];
            var succeed = Guid.TryParse(idStr, out Guid result);
            if (!succeed)
                throw new Exception("MoveResultMessage: fail to parse string: " + idStr + " to GUID.");

            userId = result;
            playerName = strArray[2];
            Int32.TryParse(strArray[3], out posX);
            Int32.TryParse(strArray[4], out posY);
            errorMessage = strArray[5];
            errorCode = strArray[6];
        }

        public override byte[] GetByteArray()
        {
            var sep = MyMessageHelper.paramSep;

            var type = ((int)Type).ToString();
            string str = type + sep + userId.ToString() +
                sep + playerName +
                sep + posX.ToString() +
                sep + posY.ToString() +
                sep + errorMessage + sep + errorCode;
            var buffer = System.Text.Encoding.UTF8.GetBytes(str);
            return buffer;
        }
    }
}
