﻿
namespace ClientWinForm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.hostHeaderLabel = new System.Windows.Forms.Label();
            this.registerUserButton = new System.Windows.Forms.Button();
            this.hostTextBox = new System.Windows.Forms.TextBox();
            this.portTextBox = new System.Windows.Forms.TextBox();
            this.portHeaderLabel = new System.Windows.Forms.Label();
            this.getUserButton = new System.Windows.Forms.Button();
            this.serverMessageTextBox = new System.Windows.Forms.TextBox();
            this.userNameTextBox = new System.Windows.Forms.TextBox();
            this.userNameHeaderLabel = new System.Windows.Forms.Label();
            this.guidHeaderLabel = new System.Windows.Forms.Label();
            this.guidTextBox = new System.Windows.Forms.TextBox();
            this.serverMessageHeaderLabel = new System.Windows.Forms.Label();
            this.getPositionButton = new System.Windows.Forms.Button();
            this.getNearestPlayer = new System.Windows.Forms.Button();
            this.upButton = new System.Windows.Forms.Button();
            this.leftButton = new System.Windows.Forms.Button();
            this.downButton = new System.Windows.Forms.Button();
            this.rightButton = new System.Windows.Forms.Button();
            this.gameBoardHeaderText = new System.Windows.Forms.Label();
            this.gameBoardText = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // hostHeaderLabel
            // 
            this.hostHeaderLabel.AutoSize = true;
            this.hostHeaderLabel.Location = new System.Drawing.Point(189, 105);
            this.hostHeaderLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.hostHeaderLabel.Name = "hostHeaderLabel";
            this.hostHeaderLabel.Size = new System.Drawing.Size(43, 20);
            this.hostHeaderLabel.TabIndex = 0;
            this.hostHeaderLabel.Text = "Host";
            // 
            // registerUserButton
            // 
            this.registerUserButton.Location = new System.Drawing.Point(570, 97);
            this.registerUserButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.registerUserButton.Name = "registerUserButton";
            this.registerUserButton.Size = new System.Drawing.Size(182, 89);
            this.registerUserButton.TabIndex = 1;
            this.registerUserButton.Text = "Connect and Register";
            this.registerUserButton.UseVisualStyleBackColor = true;
            this.registerUserButton.Click += new System.EventHandler(this.registerUserButton_Click);
            // 
            // hostTextBox
            // 
            this.hostTextBox.Location = new System.Drawing.Point(242, 100);
            this.hostTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.hostTextBox.MaxLength = 15;
            this.hostTextBox.Name = "hostTextBox";
            this.hostTextBox.ReadOnly = true;
            this.hostTextBox.Size = new System.Drawing.Size(139, 26);
            this.hostTextBox.TabIndex = 2;
            this.hostTextBox.Text = "127.0.0.1";
            // 
            // portTextBox
            // 
            this.portTextBox.Location = new System.Drawing.Point(242, 155);
            this.portTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.portTextBox.MaxLength = 10;
            this.portTextBox.Name = "portTextBox";
            this.portTextBox.ReadOnly = true;
            this.portTextBox.Size = new System.Drawing.Size(139, 26);
            this.portTextBox.TabIndex = 3;
            this.portTextBox.Text = "8888";
            // 
            // portHeaderLabel
            // 
            this.portHeaderLabel.AutoSize = true;
            this.portHeaderLabel.Location = new System.Drawing.Point(189, 160);
            this.portHeaderLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.portHeaderLabel.Name = "portHeaderLabel";
            this.portHeaderLabel.Size = new System.Drawing.Size(42, 20);
            this.portHeaderLabel.TabIndex = 4;
            this.portHeaderLabel.Text = "Port:";
            // 
            // getUserButton
            // 
            this.getUserButton.Location = new System.Drawing.Point(760, 97);
            this.getUserButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.getUserButton.Name = "getUserButton";
            this.getUserButton.Size = new System.Drawing.Size(182, 89);
            this.getUserButton.TabIndex = 5;
            this.getUserButton.Text = "Get User";
            this.getUserButton.UseVisualStyleBackColor = true;
            this.getUserButton.Click += new System.EventHandler(this.getUserButton_Click);
            // 
            // serverMessageTextBox
            // 
            this.serverMessageTextBox.Location = new System.Drawing.Point(242, 351);
            this.serverMessageTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.serverMessageTextBox.MaxLength = 150;
            this.serverMessageTextBox.Multiline = true;
            this.serverMessageTextBox.Name = "serverMessageTextBox";
            this.serverMessageTextBox.ReadOnly = true;
            this.serverMessageTextBox.Size = new System.Drawing.Size(562, 51);
            this.serverMessageTextBox.TabIndex = 6;
            // 
            // userNameTextBox
            // 
            this.userNameTextBox.Location = new System.Drawing.Point(242, 212);
            this.userNameTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.userNameTextBox.MaxLength = 15;
            this.userNameTextBox.Name = "userNameTextBox";
            this.userNameTextBox.Size = new System.Drawing.Size(320, 26);
            this.userNameTextBox.TabIndex = 7;
            // 
            // userNameHeaderLabel
            // 
            this.userNameHeaderLabel.AutoSize = true;
            this.userNameHeaderLabel.Location = new System.Drawing.Point(138, 217);
            this.userNameHeaderLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.userNameHeaderLabel.Name = "userNameHeaderLabel";
            this.userNameHeaderLabel.Size = new System.Drawing.Size(93, 20);
            this.userNameHeaderLabel.TabIndex = 8;
            this.userNameHeaderLabel.Text = "User Name:";
            // 
            // guidHeaderLabel
            // 
            this.guidHeaderLabel.AutoSize = true;
            this.guidHeaderLabel.Location = new System.Drawing.Point(168, 266);
            this.guidHeaderLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.guidHeaderLabel.Name = "guidHeaderLabel";
            this.guidHeaderLabel.Size = new System.Drawing.Size(51, 20);
            this.guidHeaderLabel.TabIndex = 9;
            this.guidHeaderLabel.Text = "GUID";
            // 
            // guidTextBox
            // 
            this.guidTextBox.Location = new System.Drawing.Point(242, 262);
            this.guidTextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.guidTextBox.MaxLength = 30;
            this.guidTextBox.Name = "guidTextBox";
            this.guidTextBox.ReadOnly = true;
            this.guidTextBox.Size = new System.Drawing.Size(320, 26);
            this.guidTextBox.TabIndex = 10;
            // 
            // serverMessageHeaderLabel
            // 
            this.serverMessageHeaderLabel.AutoSize = true;
            this.serverMessageHeaderLabel.Location = new System.Drawing.Point(91, 354);
            this.serverMessageHeaderLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.serverMessageHeaderLabel.Name = "serverMessageHeaderLabel";
            this.serverMessageHeaderLabel.Size = new System.Drawing.Size(127, 20);
            this.serverMessageHeaderLabel.TabIndex = 11;
            this.serverMessageHeaderLabel.Text = "Latest Message:";
            // 
            // getPositionButton
            // 
            this.getPositionButton.Location = new System.Drawing.Point(570, 199);
            this.getPositionButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.getPositionButton.Name = "getPositionButton";
            this.getPositionButton.Size = new System.Drawing.Size(182, 89);
            this.getPositionButton.TabIndex = 12;
            this.getPositionButton.Text = "Get Position";
            this.getPositionButton.UseVisualStyleBackColor = true;
            this.getPositionButton.Click += new System.EventHandler(this.getPositionButton_Click);
            // 
            // getNearestPlayer
            // 
            this.getNearestPlayer.Location = new System.Drawing.Point(760, 199);
            this.getNearestPlayer.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.getNearestPlayer.Name = "getNearestPlayer";
            this.getNearestPlayer.Size = new System.Drawing.Size(182, 89);
            this.getNearestPlayer.TabIndex = 13;
            this.getNearestPlayer.Text = "Get Nearest Player";
            this.getNearestPlayer.UseVisualStyleBackColor = true;
            this.getNearestPlayer.Click += new System.EventHandler(this.getNearestPlayer_Click);
            // 
            // upButton
            // 
            this.upButton.Location = new System.Drawing.Point(729, 439);
            this.upButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.upButton.Name = "upButton";
            this.upButton.Size = new System.Drawing.Size(75, 73);
            this.upButton.TabIndex = 14;
            this.upButton.Text = "UP";
            this.upButton.UseVisualStyleBackColor = true;
            this.upButton.Click += new System.EventHandler(this.upButton_Click);
            // 
            // leftButton
            // 
            this.leftButton.Location = new System.Drawing.Point(656, 510);
            this.leftButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.leftButton.Name = "leftButton";
            this.leftButton.Size = new System.Drawing.Size(75, 73);
            this.leftButton.TabIndex = 15;
            this.leftButton.Text = "LEFT";
            this.leftButton.UseVisualStyleBackColor = true;
            this.leftButton.Click += new System.EventHandler(this.leftButton_Click);
            // 
            // downButton
            // 
            this.downButton.Location = new System.Drawing.Point(729, 581);
            this.downButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.downButton.Name = "downButton";
            this.downButton.Size = new System.Drawing.Size(75, 73);
            this.downButton.TabIndex = 16;
            this.downButton.Text = "DOWN";
            this.downButton.UseVisualStyleBackColor = true;
            this.downButton.Click += new System.EventHandler(this.downButton_Click);
            // 
            // rightButton
            // 
            this.rightButton.Location = new System.Drawing.Point(802, 510);
            this.rightButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rightButton.Name = "rightButton";
            this.rightButton.Size = new System.Drawing.Size(75, 73);
            this.rightButton.TabIndex = 17;
            this.rightButton.Text = "RIGHT";
            this.rightButton.UseVisualStyleBackColor = true;
            this.rightButton.Click += new System.EventHandler(this.rightButton_Click);
            // 
            // gameBoardHeaderText
            // 
            this.gameBoardHeaderText.AutoSize = true;
            this.gameBoardHeaderText.Location = new System.Drawing.Point(127, 415);
            this.gameBoardHeaderText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.gameBoardHeaderText.Name = "gameBoardHeaderText";
            this.gameBoardHeaderText.Size = new System.Drawing.Size(104, 20);
            this.gameBoardHeaderText.TabIndex = 19;
            this.gameBoardHeaderText.Text = "Game Board:";
            // 
            // gameBoardText
            // 
            this.gameBoardText.Enabled = false;
            this.gameBoardText.Location = new System.Drawing.Point(242, 412);
            this.gameBoardText.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gameBoardText.MaxLength = 1000;
            this.gameBoardText.Multiline = true;
            this.gameBoardText.Name = "gameBoardText";
            this.gameBoardText.ReadOnly = true;
            this.gameBoardText.Size = new System.Drawing.Size(380, 344);
            this.gameBoardText.TabIndex = 18;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1042, 938);
            this.Controls.Add(this.gameBoardHeaderText);
            this.Controls.Add(this.gameBoardText);
            this.Controls.Add(this.rightButton);
            this.Controls.Add(this.downButton);
            this.Controls.Add(this.leftButton);
            this.Controls.Add(this.upButton);
            this.Controls.Add(this.getNearestPlayer);
            this.Controls.Add(this.getPositionButton);
            this.Controls.Add(this.serverMessageHeaderLabel);
            this.Controls.Add(this.guidTextBox);
            this.Controls.Add(this.guidHeaderLabel);
            this.Controls.Add(this.userNameHeaderLabel);
            this.Controls.Add(this.userNameTextBox);
            this.Controls.Add(this.serverMessageTextBox);
            this.Controls.Add(this.getUserButton);
            this.Controls.Add(this.portHeaderLabel);
            this.Controls.Add(this.portTextBox);
            this.Controls.Add(this.hostTextBox);
            this.Controls.Add(this.registerUserButton);
            this.Controls.Add(this.hostHeaderLabel);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "Client";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label hostHeaderLabel;
        private System.Windows.Forms.Button registerUserButton;
        private System.Windows.Forms.TextBox hostTextBox;
        private System.Windows.Forms.TextBox portTextBox;
        private System.Windows.Forms.Label portHeaderLabel;
        private System.Windows.Forms.Button getUserButton;
        private System.Windows.Forms.TextBox serverMessageTextBox;
        private System.Windows.Forms.TextBox userNameTextBox;
        private System.Windows.Forms.Label userNameHeaderLabel;
        private System.Windows.Forms.Label guidHeaderLabel;
        private System.Windows.Forms.TextBox guidTextBox;
        private System.Windows.Forms.Label serverMessageHeaderLabel;
        private System.Windows.Forms.Button getPositionButton;
        private System.Windows.Forms.Button getNearestPlayer;
        private System.Windows.Forms.Button upButton;
        private System.Windows.Forms.Button leftButton;
        private System.Windows.Forms.Button downButton;
        private System.Windows.Forms.Button rightButton;
        private System.Windows.Forms.Label gameBoardHeaderText;
        private System.Windows.Forms.TextBox gameBoardText;
    }
}

