﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Collections.Concurrent;
using System.Windows.Media;
using ClassLib;
using System.Diagnostics;

namespace ClientWinForm
{
    public partial class Form1 : Form
    {

        private MyNetworkManager networkManager = new MyNetworkManager();

        private bool preventButtonToReceiveCommand;

        public Form1()
        {
            InitializeComponent();

            networkManager.AddOnNetworkPhaseChanged(OnNetworkPhaseChanged);
            networkManager.AddOnClientPrintMessage(OnClientPrintMessage);
            networkManager.AddOnUserRegistered(OnUserRegistered);
            networkManager.AddOnGameBoardMustRefresh(OnGameBoardMustRefresh);

            // always start with not connect state.
            ShowNotConnectDisplayState();
        }

        #region Callbacks

        private void OnClientPrintMessage(string message)
        {
            serverMessageTextBox.Text = message;
        }

        private void OnNetworkPhaseChanged(NetworkPhase oldPhase, NetworkPhase newPhase)
        {
            switch (newPhase)
            {
                case NetworkPhase.NotConnected:
                    ShowNotConnectDisplayState();
                    break;

                case NetworkPhase.Registered:
                    ShowRegisteredDisplayState();
                    break;

                case NetworkPhase.WaitForResponse:
                    ShowWaitingForResponseState();
                    break;

                default:
                    Trace.TraceError("Form1: unsupported phase to update display state: " + newPhase);
                    break;
            }
        }

        private void OnUserRegistered()
        {
            userNameTextBox.Text = networkManager.PlayerName;
            guidTextBox.Text = networkManager.UserID.ToString();
        }

        private void OnGameBoardMustRefresh()
        {
            var text = networkManager.GetGameBoardText();
            gameBoardText.Text = text;
        }

        #endregion

        #region Show Display State

        public void ShowNotConnectDisplayState()
        {
            preventButtonToReceiveCommand = false;

            userNameTextBox.Text = string.Empty;
            guidTextBox.Text = string.Empty;
            gameBoardText.Text = string.Empty;

            userNameTextBox.ReadOnly = false;
            registerUserButton.Enabled = true;

            getUserButton.Enabled = false;
            getPositionButton.Enabled = false;
            getNearestPlayer.Enabled = false;

            upButton.Enabled = false;
            downButton.Enabled = false;
            leftButton.Enabled = false;
            rightButton.Enabled = false;
        }

        public void ShowRegisteredDisplayState()
        {
            preventButtonToReceiveCommand = false;

            userNameTextBox.ReadOnly = true;
            registerUserButton.Enabled = false;

            getUserButton.Enabled = true;
            getPositionButton.Enabled = true;
            getNearestPlayer.Enabled = true;

            upButton.Enabled = true;
            downButton.Enabled = true;
            leftButton.Enabled = true;
            rightButton.Enabled = true;
        }

        public void ShowWaitingForResponseState()
        {
            userNameTextBox.ReadOnly = true;
            registerUserButton.Enabled = false;

            preventButtonToReceiveCommand = true;
        }

        #endregion

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        #region Button clicks

        private void registerUserButton_Click(object sender, EventArgs e)
        {
            if (preventButtonToReceiveCommand)
                return;

            networkManager.ConnectAndRegister(userNameTextBox.Text);
        }

        private void getUserButton_Click(object sender, EventArgs e)
        {
            if (preventButtonToReceiveCommand)
                return;

            networkManager.GetUserData(networkManager.UserID);
        }

        private void getPositionButton_Click(object sender, EventArgs e)
        {
            if (preventButtonToReceiveCommand)
                return;

            networkManager.GetPosition(networkManager.UserID);
        }

        private void getNearestPlayer_Click(object sender, EventArgs e)
        {
            if (preventButtonToReceiveCommand)
                return;

            networkManager.GetNearestPlayer(networkManager.UserID);
        }

        private void upButton_Click(object sender, EventArgs e)
        {
            if (preventButtonToReceiveCommand)
                return;

            networkManager.Move(networkManager.UserID, MoveDir.Up);
        }

        private void leftButton_Click(object sender, EventArgs e)
        {
            if (preventButtonToReceiveCommand)
                return;

            networkManager.Move(networkManager.UserID, MoveDir.Left);
        }

        private void downButton_Click(object sender, EventArgs e)
        {
            if (preventButtonToReceiveCommand)
                return;

            networkManager.Move(networkManager.UserID, MoveDir.Down);
        }

        private void rightButton_Click(object sender, EventArgs e)
        {
            if (preventButtonToReceiveCommand)
                return;

            networkManager.Move(networkManager.UserID, MoveDir.Right);
        }

        #endregion
    }
}
