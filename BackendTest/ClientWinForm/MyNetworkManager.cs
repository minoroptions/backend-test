﻿using ClassLib;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;

namespace ClientWinForm
{
    public enum NetworkPhase
    {
        NotConnected,
        Registered,
        WaitForResponse,
    }

    public class MyNetworkManager
    {
        #region Const Values

        public const int gridSizeX = 10;

        public const int gridSizeY = 10;

        #endregion

        #region Contructor and Destructor

        public MyNetworkManager()
        {
            CompositionTarget.Rendering += CompositionTargetUpdate;
        }

        ~MyNetworkManager()
        {
            CompositionTarget.Rendering -= CompositionTargetUpdate;
        }

        #endregion

        #region Network Phase and Connection

        public const int port = 8888;

        private NetworkPhase networkPhase = NetworkPhase.NotConnected;
        public NetworkPhase NetworkPhase => networkPhase;

        private bool isConnected;

        private Socket socket;

        private NetworkStream networkStream;

        private bool isOnConnectionLost;

        #endregion

        #region Server Listening Tasks

        private ConcurrentQueue<MyMessageBase> receivedMessageQueue = new ConcurrentQueue<MyMessageBase>();

        private CancellationTokenSource serverListenerSource;

        #endregion

        #region User Data for Player and Opponent

        private string cachedPlayerName;

        private UserData playerUserData;
        public UserData PlayerUserData => playerUserData;

        private UserData opponentUserData;
        public UserData OpponentUserData => opponentUserData;

        public Guid UserID
        {
            get 
            {
                if (playerUserData == null)
                    return Guid.Empty;

                return playerUserData.userId;
            }
        }

        public string PlayerName
        {
            get
            {
                if (playerUserData == null)
                    return string.Empty;

                return playerUserData.playerName;
            }
        }

        private void AddPlayer(Guid id, string name, int posX, int posY)
        {
            Trace.Assert(playerUserData == null,
                "MyNetworkManager: playerUserData must be null when calling AddPlayer().");

            playerUserData = new UserData
            {
                userId = id,
                playerName = name,
                posX = posX,
                posY = posY,
            };

            onUserRegistered?.Invoke();
            onGameBoardMustRefresh?.Invoke();
        }

        private void UpdatePlayerPos(int posX, int posY)
        {
            playerUserData.posX = posX;
            playerUserData.posY = posY;

            onGameBoardMustRefresh?.Invoke();
        }

        private void AddOpponentOrUpdatePos(Guid id, string name, int posX, int posY)
        {
            if (opponentUserData == null)
            {
                opponentUserData = new UserData
                {
                    userId = id,
                    playerName = name,
                    posX = posX,
                    posY = posY,
                };

                onGameBoardMustRefresh?.Invoke();
                return;
            }

            Trace.Assert(opponentUserData.userId == id, 
                "MyNetworkManager: opponentUserData id should be the same from result message.");

            opponentUserData.posX = posX;
            opponentUserData.posY = posY;

            onGameBoardMustRefresh?.Invoke();
        }

        private void RemoveOpponent()
        {
            opponentUserData = null;

            onGameBoardMustRefresh?.Invoke();
        }

        public string GetGameBoardText()
        {
            string text = string.Empty;

            for (var y = 0; y < gridSizeY; y++)
            {
                for (var x = 0; x < gridSizeX; x++)
                {
                    if (IsPlayerPos(x, y))
                    {
                        text += "[P]";
                        continue;
                    }
                    
                    if (IsOpponentPos(x, y))
                    {
                        text += "[O]";
                        continue;
                    }

                    text += "[_]";
                }

                text += Environment.NewLine;
            }

            return text;
        }

        private bool IsPlayerPos(int posX, int posY)
        {
            if (playerUserData == null)
                return false;

            return posX == playerUserData.posX && posY == playerUserData.posY;
        }

        private bool IsOpponentPos(int posX, int posY)
        {
            if (opponentUserData == null)
                return false;

            return posX == opponentUserData.posX && posY == opponentUserData.posY;
        }

        #endregion


        #region Events and Callbacks

        private Action<NetworkPhase, NetworkPhase> onNetworkPhaseChanged;

        public void AddOnNetworkPhaseChanged(Action<NetworkPhase, NetworkPhase> callback)
        {
            onNetworkPhaseChanged += callback;
        }

        public void RemoveOnNetworkPhaseChanged(Action<NetworkPhase, NetworkPhase> callback)
        {
            onNetworkPhaseChanged -= callback;
        }

        private Action<string> onClientPrintMessage;

        public void AddOnClientPrintMessage(Action<string> callback)
        {
            onClientPrintMessage += callback;
        }

        public void RemoveOnClientPrintMessage(Action<string> callback)
        {
            onClientPrintMessage -= callback;
        }

        private Action onUserRegistered;

        public void AddOnUserRegistered(Action callback)
        {
            onUserRegistered += callback;
        }

        public void RemoveOnUserRegistered(Action callback)
        {
            onUserRegistered -= callback;
        }

        private Action onGameBoardMustRefresh;

        public void AddOnGameBoardMustRefresh(Action callback)
        {
            onGameBoardMustRefresh += callback;
        }

        public void RemoveOnGameBoardMustRefresh(Action callback)
        {
            onGameBoardMustRefresh -= callback;
        }

        #endregion

        private void LogClientMessageOnForm(string message)
        {
            Console.WriteLine(message);
            onClientPrintMessage?.Invoke(message);
        }

        #region Init and Dispose Network

        private void InitSocketStreamAndConnect()
        {
            if (isConnected)
                return;

            try
            {
                isConnected = true;

                var endPoint = new IPEndPoint(IPAddress.Loopback, port);
                socket = new Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                socket.Connect(endPoint);
                networkStream = new NetworkStream(socket, true);
            }
            catch (Exception ex)
            {
                LogClientMessageOnForm("MyNetworkManager: fail to connect to the server.");
                OnConnectionLost();
            }
        }

        private void DisposeStreamAndSocket()
        {
            try
            {
                if (networkStream != null)
                    networkStream.Close();

                if (socket != null)
                    socket.Shutdown(SocketShutdown.Both);
            }
            catch (Exception ex)
            {

            }

            networkStream = null;
            socket = null;

            Console.WriteLine("MyNetworkManager: networkStream and socket disposed.");
        }

        private void OnConnectionLost()
        {
            isOnConnectionLost = false;

            if (!isConnected)
                return;

            isConnected = false;

            CancelServerListenerTask();

            DisposeStreamAndSocket();

            receivedMessageQueue = new ConcurrentQueue<MyMessageBase>();

            playerUserData = null;

            opponentUserData = null;

            if (networkPhase != NetworkPhase.NotConnected)
                SetNetworkPhase(NetworkPhase.NotConnected);
        }

        #endregion


        #region Network API Commands

        public void ConnectAndRegister(string playerName)
        {
            if (isConnected)
            {
                LogClientMessageOnForm("MyNetworkManager: already connected fail to ConnectAndRegister()");
                return;
            }

            if (networkPhase == NetworkPhase.WaitForResponse)
                return;

            Trace.Assert(networkPhase == NetworkPhase.NotConnected, 
                "MyNetworkManager: networkPhase has to be NotConnected before ConnectAndRegister()");

            InitSocketStreamAndConnect();

            if (!isConnected)
                return;

            StartServerListenerTask();

            SetNetworkPhase(NetworkPhase.WaitForResponse);

            SendRegisterMessage(playerName);
        }

        private string CleanUpPlayerName(string playerName)
        {
            var result = playerName.Replace(MyMessageHelper.paramSep.ToString(), string.Empty);

            if (string.IsNullOrEmpty(result))
                return "Default";

            return result;
        }

        private void SendRegisterMessage(string playerName)
        {
            Trace.Assert(isConnected, "MyNetworkManager: cannot SendRegisterMessage, when isConnected is false.");

            playerName = CleanUpPlayerName(playerName);
            cachedPlayerName = playerName;

            var message = new RegisterUserMessage(playerName);
            SendMessage(message);
        }

        public void GetUserData(Guid userId)
        {
            if (networkPhase != NetworkPhase.Registered || !isConnected)
                return;

            SetNetworkPhase(NetworkPhase.WaitForResponse);

            var message = new GetUserMessage(userId);
            SendMessage(message);
        }

        public void GetPosition(Guid userId)
        {
            if (networkPhase != NetworkPhase.Registered || !isConnected)
                return;

            SetNetworkPhase(NetworkPhase.WaitForResponse);

            var message = new GetPositionMessage(userId);
            SendMessage(message);
        }

        public void GetNearestPlayer(Guid yourUserId)
        {
            if (networkPhase != NetworkPhase.Registered || !isConnected)
                return;

            SetNetworkPhase(NetworkPhase.WaitForResponse);

            var message = new GetNearestMessage(yourUserId);
            SendMessage(message);
        }

        public void Move(Guid userId, MoveDir moveDir)
        {
            if (networkPhase != NetworkPhase.Registered || !isConnected)
                return;

            SetNetworkPhase(NetworkPhase.WaitForResponse);

            var message = new MoveMessage(userId, moveDir);
            SendMessage(message);
        }

        #region Send Message and Network Phase Changing

        private void SetNetworkPhase(NetworkPhase newPhase)
        {
            if (newPhase == networkPhase)
            {
                Trace.TraceError("MyNetworkManager: fail to SetNetworkPhase(" + newPhase + ") because it is the same as the current.");
                return;
            }

            var previousPhase = networkPhase;
            networkPhase = newPhase;
            onNetworkPhaseChanged?.Invoke(previousPhase, networkPhase);
        }

        private void SendMessage(MyMessageBase message)
        {
            try
            {
                var buffer = message.GetByteArray();
                networkStream.Write(buffer, 0, buffer.Length);
            }
            catch (Exception ex)
            {
                LogClientMessageOnForm("MyNetworkManager: error while sending a message, most likely the server is gone.");
                OnConnectionLost();
            }
        }

        #endregion

        #endregion


        #region Server Listener Task and Methods

        private void StartServerListenerTask()
        {
            CancelServerListenerTask();

            serverListenerSource = new CancellationTokenSource();

            var token = serverListenerSource.Token;
            _ = Task.Run(() => UpdateServerListenerTask(token), token);
        }

        private void CancelServerListenerTask()
        {
            if (serverListenerSource == null)
                return;

            serverListenerSource.Cancel();

            serverListenerSource = null;
        }

        private async void UpdateServerListenerTask(CancellationToken token)
        {
            var response = new byte[1024];

            while (true)
            {
                try
                {
                    token.ThrowIfCancellationRequested();

                    var bytesRead = await networkStream.ReadAsync(response, 0, response.Length);
                    if (bytesRead == 0)
                        continue;

                    token.ThrowIfCancellationRequested();

                    var message = MyMessageHelper.GetMyMessage(response, bytesRead);
                    receivedMessageQueue.Enqueue(message);

                    Console.WriteLine("MyNetworkManager: server message received: " + message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("MyNetworkManager: error while reading server message, most likely the server is gone.");
                    isOnConnectionLost = true;
                    break;
                }

            } 
        }

        #endregion

        #region Update That Syncs With Winform and Process Messages

        private void CompositionTargetUpdate(object sender, EventArgs e)
        {
            if (isOnConnectionLost)
            {
                OnConnectionLost();
                return;
            }

            if (receivedMessageQueue.Count == 0)
                return;

            var succeed = receivedMessageQueue.TryDequeue(out MyMessageBase message);
            if (!succeed)
                return;

            Console.WriteLine("MyNetworkManager: get a message str from queue: " + message.Type);

            try
            {
                ProcessIncomingServerMessage(message);
            }
            catch (Exception ex)
            {
                LogClientMessageOnForm("MyNetworkManager: error while ProcessIncomingServerMessage().");
                OnConnectionLost();
            }
        }

        private void ProcessIncomingServerMessage(MyMessageBase message)
        {
            switch (message.Type)
            {
                case MyMessageType.Disconnect:
                    ProcessDisconnectMessage(message as DisconnectMessage);
                    break;

                case MyMessageType.RegisterResult:
                    ProcessRegisterResult(message as RegisterResultMessage);
                    break;

                case MyMessageType.GetUserResult:
                    ProcessGetUserResult(message as GetUserResultMessage);
                    break;

                case MyMessageType.GetPositionResult:
                    ProcessGetPositionResult(message as GetPositionResultMessage);
                    break;

                case MyMessageType.GetNearestResult:
                    ProcessGetNearestResult(message as GetNearestResultMessage);
                    break;

                case MyMessageType.MoveResult:
                    ProcessMoveResult(message as MoveResultMessage);
                    break;

                default:
                    {
                        Trace.TraceError("MyNetworkManager: fail to ProcessIncomingServerMessage type: " + message.Type);
                        OnConnectionLost();
                    }
                    break;
            }
        }

        private void ProcessDisconnectMessage(DisconnectMessage message)
        {
            if (message.UserID == Guid.Empty)
            {
                Trace.TraceError("MyNetworkManager: this is weird, userID should not be empty.");
                return;
            }

            RemoveOpponent();
        }

        private void ProcessRegisterResult(RegisterResultMessage message)
        {
            if (!message.IsSuccess)
            {
                LogClientMessageOnForm("Register() failed code: " + message.ErrorCode + ", " + message.ErrorMessage);
                OnConnectionLost();
                return;
            }

            AddPlayer(message.UserId, cachedPlayerName, message.PosX, message.PosY);
            LogClientMessageOnForm("Register() success, id: " + message.UserId);
            SetNetworkPhase(NetworkPhase.Registered);
        }

        private void ProcessGetUserResult(GetUserResultMessage message)
        {
            SetNetworkPhase(NetworkPhase.Registered);

            if (!message.IsSuccess)
            {
                LogClientMessageOnForm("GetUser() failed, code: " + message.ErrorCode + ", " + message.ErrorMessage);
                return;
            }

            LogClientMessageOnForm("GetUser() success, id: " + message.UserId);
        }

        private void ProcessGetPositionResult(GetPositionResultMessage message)
        {
            SetNetworkPhase(NetworkPhase.Registered);

            if (!message.IsSuccess)
            {
                LogClientMessageOnForm("GetPosition() failed, code: " + message.ErrorCode + ", " + message.ErrorMessage);
                return;
            }

            LogClientMessageOnForm("GetPosition() success, PosX: " + message.PosX + ", PosY: " + message.PosY);
        }

        private void ProcessGetNearestResult(GetNearestResultMessage message)
        {
            SetNetworkPhase(NetworkPhase.Registered);

            if (!message.IsSuccess)
            {
                LogClientMessageOnForm("GetNearestPlayer() failed, code: " + message.ErrorCode + ", " + message.ErrorMessage);
                return;
            }

            LogClientMessageOnForm("GetNearestPlayer() success, Distance: " + message.Distance);
        }

        private void ProcessMoveResult(MoveResultMessage message)
        {
            SetNetworkPhase(NetworkPhase.Registered);

            if (!message.IsSuccess)
            {
                LogClientMessageOnForm("Move() failed, code: " + message.ErrorCode + ", " + message.ErrorMessage);
                return;
            }

            if (UserID == message.UserId)
            {
                UpdatePlayerPos(message.PosX, message.PosY);
                LogClientMessageOnForm("Move() for user success, posX: " + message.PosX + ", posY: " + message.PosY);
                return;
            }

            if (OpponentUserData == null)
                LogClientMessageOnForm("Spawn opponent, posX: " + message.PosX + ", posY: " + message.PosY);
            else
                LogClientMessageOnForm("Move() for opponent, posX: " + message.PosX + ", posY: " + message.PosY);

            AddOpponentOrUpdatePos(message.UserId, message.PlayerName, message.PosX, message.PosY);
        }

        #endregion
    }
}
